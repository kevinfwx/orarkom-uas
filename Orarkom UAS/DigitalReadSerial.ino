/*
  DigitalReadSerial

  Reads a digital input on pin 2, prints the result to the Serial Monitor

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/DigitalReadSerial
*/
#define BLYNK_TEMPLATE_ID "TMPLJxu2zQdf"
#define BLYNK_DEVICE_NAME "Simulasi palang pintu"
#define BLYNK_AUTH_TOKEN "Q4bNkGQQV5ozXW2Pb095hl06C6DuO3J0"
/* Comment this out to disable prints and save space */
#define BLYNK_PRINT Serial
#define IR_PIN D1

#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "Q4bNkGQQV5ozXW2Pb095hl06C6DuO3J0";

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "Billa";
char pass[] = "bila12345";
// digital pin 2 has a pushbutton attached to it. Give it a name:
int pushButton = D0;

// the setup routine runs once when you press reset:
void setup() {
  pinMode(IR_PIN, INPUT);
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  Blynk.begin(auth, ssid, pass, "Blynk.Cloud");

}

// the loop routine runs over and over again forever:
void loop() {
  Blynk.run();
  // read the input pin:
   int buttonState = digitalRead(IR_PIN);

  // Cek nilai pin
  if (buttonState == 1) 
   {
    Serial.println("buka");
     Blynk.virtualWrite(V0, 0);
  } 
   else 
   {
    Serial.println("tutup");
     Blynk.virtualWrite(V0, 1);
  }

}
